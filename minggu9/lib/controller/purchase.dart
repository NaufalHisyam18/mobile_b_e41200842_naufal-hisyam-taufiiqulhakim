import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:minggu9/model/product.dart';

class Purchase extends GetxController {
  var products = <Product>[].obs;
  @override
  void onInit() {
    fetchProducts();
    super.onInit();
  }

  void fetchProducts() async {
    await Future.delayed(Duration(seconds: 1));

    var serverResponse = [
      // Product(
      //     1,
      //     "New Iphone",
      //     "https://tse3.mm.bing.net/th?id=OIP.NmEbhQrbUO9uR2kttP_WsQHaD5&pid=Api&P=0&w=345&h=182",
      //     "Let's bring youth to the next level ",
      //     300.0),
      Product(
          1,
          "New Iphone",
          "https://images.unsplash.com/photo-1644982654131-79f434ac0c6c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80",
          "Let's bring youth to the next level",
          300.0),
      Product(
          1,
          "Demo Product",
          "https://images.unsplash.com/photo-1638913665258-ddd2bceafb30?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80",
          "This is a Product that we are going to show by getx",
          300.0),
      Product(
          1,
          "Demo Product",
          "https://images.unsplash.com/photo-1648737154547-b0dfd281c51e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80",
          "This is a Product that we are going to show by getx",
          300.0),
      // Product(1, "Demo Product", "aby",
      //     "This is a Product that we are going to show by getx", 300.0),
      // Product(1, "Demo Product", "aby",
      //     "This is a Product that we are going to show by getx", 300.0),
    ];

    products.value = serverResponse;
  }
}
