import 'dart:async';

void main() {
  print("Team - Lorde");
  print("------------");

  Timer(Duration(seconds: 3), () => print("Even the comatose"));
  Timer(Duration(seconds: 5), () => print("They don't dance and tell"));
  Timer(Duration(seconds: 7),
      () => print("We live in cities you'll never see on-screen"));
  Timer(Duration(seconds: 9),
      () => print("Not very pretty, but we sure know how to run things"));
  Timer(Duration(seconds: 11),
      () => print("Livin' in ruins of a palace within my dreams"));
  Timer(Duration(seconds: 13),
      () => print("And you know we're on each other's team"));
}
