import 'package:flutter/material.dart';
import 'package:row_coloumn/route/route.dart';

void main() {
  runApp(MaterialApp(
    onGenerateRoute: RouteGenerator.generateRoute,
  ));
}
